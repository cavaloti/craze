import React from 'react';
import "../assets/styles/index.scss";
import {Grid, Row, Col, Well, Glyphicon} from 'react-bootstrap/lib';

class About extends React.Component {

  constructor(props){
    super(props);
      this.showAbout = this.showAbout.bind(this);
  }

  showAbout() {
    return (
      <Grid>
        <Row className="show-grid about">
          <Col xs={12} md={6} className="about__text">
            <h1>Whats Craze all About?</h1>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse fringilla fringilla nisl congue congue. Maecenas nec condimentum libero, at elementum mauris.<br/>
            <br/>  
            Phasellus eget nisi dapibus, ultricies nisl at, hendrerit risusuis ornare luctus id sollicitudin ante lobortis at.</p><br/>
            <br/>
            <div className="about__text__item">
                <Glyphicon glyph="glyphicon glyphicon-chevron-right" />
                <span> Lorem ipsum dolor sit amet</span><br/>
            </div>   
            <div className="about__text__item">
                <Glyphicon glyph="glyphicon glyphicon-chevron-right" />
                <span> Lorem ipsum dolor sit amet</span><br/>
            </div>  
          </Col>
          <Col xs={12} md={6} className="about__video">
          <iframe src="https://player.vimeo.com/video/264319825?color=f8efea&title=0&byline=0&portrait=0" width="100%" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
          </Col>
        </Row>
      </Grid>
    );
    
  }

  render() {
    return ( 
      <div>
      {this.showAbout()} 
      </div>
    ); 
  }

}

export default About;
