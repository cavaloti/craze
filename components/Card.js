import React from 'react';
import PropTypes from 'prop-types';

class Card extends React.Component {

  render() {

    let component = null;
    const {
      title, valueInt, valueDecimal
    } = this.props;

    component = <div 
        className="card">
        <div className="card__header card__bg card__title">
        {title}
        </div>
        <div className="card__price">
            <span className="card__value_int">{valueInt}</span>
            <div className="card__value_decimal">{valueDecimal}</div>
        </div>    
        <ul className="card__features">
                <li className="card__feature">Feature 1</li>
                <li className="card__feature">Feature 2</li>
                <li className="card__feature">Feature 3</li>
                <li className="card__feature">Feature 4</li>
                <li className="card__feature">Feature 5</li>
            </ul>
        
        <div className="card__bg">
            <button className="card__purchase">PURCHASE</button>
        </div>
        </div>;
    return component;
  }
}

Card.propTypes = {
  cssClass: PropTypes.string,
  title:PropTypes.string,
  valueInt:PropTypes.string,
  valueDecimal:PropTypes.string
};

export default Card;
