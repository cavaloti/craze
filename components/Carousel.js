import React from "react";
import "../assets/styles/index.scss";
import "../assets/styles/_carousel.scss";
import { Grid, Row, Col } from "react-bootstrap/lib";

class Carousel extends React.Component {
  state = {
    currentSlide: 0,
    timer: null,
    slides: [
      {
        author: "Sarah Hunt 1",
        quote:
          "“1) Craze is one of the most complete app packages I have ever come across. I would highly reccomend it to anyone!”"
      },
      {
        author: "Sarah Hunt 2",
        quote:
          "“2) Craze is one of the most complete app packages I have ever come across. I would highly reccomend it to anyone!”"
      },
      {
        author: "Sarah Hunt 3",
        quote:
          "“3) Craze is one of the most complete app packages I have ever come across. I would highly reccomend it to anyone!”"
      }
    ]
  };
  constructor(props) {
    super(props);
    this.showCarousel = this.showCarousel.bind(this);
    this.spinSlides = this.spinSlides.bind(this);
    this.goToSlide = this.goToSlide.bind(this);
  }

  componentDidMount() {
    this.spinSlides();
  }

  spinSlides() {

    const timer = setInterval(() => {
      const nextSlide =
        (this.state.currentSlide + 1) % this.state.slides.length;
      this.setState({ currentSlide: nextSlide });
    }, 4000);
    this.setState({ timer });
    
  }

  goToSlide(slideNumber) {
    this.setState({ 
      currentSlide: slideNumber,
      timer:null
    });
    clearInterval(this.state.timer);
  }

  showCarousel() {
    return (
      <Grid>
        <Row className="show-grid">
          <Col xs={12} md={12}>
            <div id="carousel">
              {this.state.slides.map((slide, index) => {
                const cssClass =
                  index === this.state.currentSlide ? "text_active" : "";
                return (
                  <div
                    className={`${cssClass} text-center carousel_text carousel_text_0`}
                  >
                    <p>{slide.quote}</p>
                    <h3>{slide.author}</h3>
                  </div>
                );
              })}
            </div>
            <div className="text-center dots-wrapper">
              <ul id="dots" className="list-inline dots">
                {this.state.slides.map((slide, index) => {
                  const cssClass =
                    index === this.state.currentSlide ? "active" : "";
                  return (
                    <li
                      data-index="0"
                      className={`${cssClass} dot`}
                      onClick={() => this.goToSlide(index)}
                    />
                  );
                })}
              </ul>
            </div>
          </Col>
        </Row>
      </Grid>
    );
  }

  render() {
    return <div>{this.showCarousel()}</div>;
  }
}

export default Carousel;
