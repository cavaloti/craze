import React from 'react';
import "../assets/styles/index.scss";
import {Grid, Row, Col} from 'react-bootstrap/lib';

class Features extends React.Component {

  constructor(props){
    super(props);
      this.showFeatures = this.showFeatures.bind(this);
  }

  showFeatures() {
    return (
      <div className="features">
        <Grid>
            <Row>
                <Col xs={12} md={12} className="features__lead">
                <h1>Craze Features</h1>
                <p>Lorem ipsum dolor sit amet, pro aeterno adipisci ex, pro no iriure accusam. Eros admodum intellegat ex mea, mei wisi nullam ne, qui cu aliquid nominavi. Est habemus maiestatis ut. Vim quod elitr interesset ea.</p>
                </Col>
            </Row>
        </Grid>

        <Grid>
            <Row className="show-grid">
                <Col xs={12} md={4} className="features__item features__item__border-right">
                    <div className="features__not_middle">
                        <img src="/static/images/icon-flat-design.jpg" alt="Flat Design" title="Flat Design" />
                        <h1>Flat Design</h1>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse fringilla.</p>
                    </div>    
                    <div className="features__border_bottom"></div>
                </Col>
                <Col xs={12} md={4} className="features__item features__item__border-right">
                    <div className="features__middle">
                        <img src="/static/images/icon-marketable.jpg" alt="Marketable" title="Marketable" />
                        <h1>Marketable</h1>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse fringilla.</p>
                    </div>
                    <div className="features__border_bottom"></div>
                </Col>
                <Col xs={12} md={4} className="features__item">
                    <div className="features__not_middle">
                        <img src="/static/images/icon-edit-photos.jpg" alt="Edit Photos" title="Edit Photos" />
                        <h1>Edit Photos</h1>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse fringilla.</p>
                    </div>
                    <div className="features__border_bottom"></div>          
                </Col>
            </Row>
        </Grid>

        <Grid>
            <Row className="show-grid">
                <Col xs={12} md={4} className="features__item features__item__border-right">
                    <div className="features__not_middle">
                        <img src="/static/images/icon-edit-colors.jpg" alt="Edit Colors" title="Edit Colors" />
                        <h1>Edit Colors</h1>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse fringilla.</p>
                    </div>
                    <div className="features__border_bottom"></div>
                </Col>
                <Col xs={12} md={4} className="features__item features__item__border-right">
                    <div className="features__middle">
                        <img src="/static/images/icon-user-friendly.jpg" alt="User Friendly" title="User Friendly" />
                        <h1>User Friendly</h1>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse fringilla.</p>
                    </div>
                    <div className="features__border_bottom"></div>
                </Col>
                <Col xs={12} md={4} className="features__item">
                    <div className="features__not_middle">
                        <img src="/static/images/icon-100-percent-editable.png" alt="100% Editable" title="100% Editable" />
                        <h1>100% Editable</h1>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse fringilla.</p>
                    </div>
                    <div className="features__border_bottom"></div>       
                </Col>
            </Row>
        </Grid>        
      </div>
    );

  }

  render() {
    return ( 
      <div>
      {this.showFeatures()} 
      </div>
    ); 
  }

}

export default Features;
