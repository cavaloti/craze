import React from 'react';
import "../assets/styles/index.scss";
import {Grid, Row, Col} from 'react-bootstrap/lib';

class Footer extends React.Component {
 
  constructor(props){
    super(props);
    this.showFooter = this.showFooter.bind(this);
  }

  showFooter() {
    return (
        <Grid>
            <Row className="footer">
                <Col xs={12} md={12} className="">
                <ul>
                    <li>
                        <a href="#">Contact</a>
                    </li>
                    <li>
                        <a href="#">Download</a>
                    </li>
                    <li>
                        <a href="#">Press</a>
                    </li>
                    <li>
                        <a href="#">Email</a>
                    </li>
                    <li>
                        <a href="#">Support</a>
                    </li>
                    <li>
                        <a href="#">Privacy Policy</a>
                    </li>
                </ul>    
                </Col>
            </Row>
        </Grid>
    );
    
  }


  render() {
    return ( 
      <div>
      {this.showFooter()} 
      </div>
    ); 
  }

}

export default Footer;
