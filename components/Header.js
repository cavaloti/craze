import React from 'react';
import "../assets/styles/index.scss";
import {Grid, Row, Col} from 'react-bootstrap/lib';

class Header extends React.Component {

  constructor(props){
    super(props);
      this.showHeader = this.showHeader.bind(this);
  }

  showHeader() {
    return (
      <Grid className="bg-header">
        <Row className="show-grid ">
          <Col xs={6} md={6} mdOffset={2} className="bg-header__craze-logo-box">
            <img src="/static/images/craze.png" />
            <p>
              A great new free psd theme to showcase <br/> 
              your new app. 
            </p>
            <div className="bg-header__devices">
              <a href="#" className="bg-header__devices__icon">
                <img src="/static/images/apple.png" alt="apple" />
              </a>
              <a href="#" className="bg-header__devices__icon icon-android">
                <img src="/static/images/android.png" alt="android" />
              </a>
              <a href="#" className="bg-header__devices__icon">
              <img src="/static/images/windows.png" alt="windows" />
              </a>
            </div>
          </Col>
          <Col xs={6}  md={4} className="bg-header__iphone-hand-box">
          </Col>
        </Row>
      </Grid>
    );

  }

  render() {
    return ( 
      <div>
      {this.showHeader()} 
      </div>
    ); 
  }

}

export default Header;
