import React from 'react';
import "../assets/styles/index.scss";
import Card from './Card';
import {Grid, Row, Col} from 'react-bootstrap/lib';

class Pricing extends React.Component {
 
  constructor(props){
    super(props);
    this.showPricing = this.showPricing.bind(this);
  }

  showPricing() {
    return (
        <div className="pricing">
      <Grid>
        <Row className="show-grid">
          <Col xs={12} md={12} className="pricing__plans"> 
            <h2>Pricing Plans</h2>
            <p>Lorem ipsum dolor sit amet, pro aeterno adipisci ex, pro no iriure accusam. Eros admodum intellegat ex mea, mei wisi nullam ne, qui cu aliquid nominavi. Est habemus maiestatis ut. Vim quod elitr interesset ea.</p>
          </Col>
        </Row>
        <Row className="pricing__wrapper">
            <Col xs={12} md={3} className="pricing__no-spacing">
                <Card title="Basic" valueInt="19" valueDecimal="99"/>
            </Col>
            <Col xs={12} md={3} className="pricing__no-spacing">
                <Card title="Standard" valueInt="39" valueDecimal="99"/>
            </Col>
            <Col xs={12} md={3} className="pricing__no-spacing pricing__elevation">
                <Card title="Deluxe" className="elevation" valueInt="59" valueDecimal="99"/>
            </Col>
            <Col xs={12} md={3} className="pricing__no-spacing">
                <Card title="Unlimited" valueInt="79" valueDecimal="99"/>
            </Col>
        </Row>
      </Grid>
      </div>
    );
    
  }


  render() {
    return ( 
      <div>
      {this.showPricing()} 
      </div>
    ); 
  }

}

export default Pricing;
