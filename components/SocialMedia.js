import React from 'react';
import "../assets/styles/index.scss";
import {Grid, Row, Col} from 'react-bootstrap/lib';

class SocialMedia extends React.Component {
 
  constructor(props){
    super(props);
    this.showSocialMedia = this.showSocialMedia.bind(this);
  }

  showSocialMedia() {
    return (
    <div>
      <Grid>
            <Row className="social-media">
                <Col xs={12} md={12} className="">
                    <h2>Say Hi & Get in Touch</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit suspendisse. </p>
                </Col>
            </Row>
        </Grid>
        <Grid>
            <Row className="social-media-items">
                <Col xs={12} md={12} className="social-media-items">
                <div>
                <a href="#" className="social-media-items__item">
                    <img src="/static/images/icon-twitter.png" alt="twitter" />
                </a>
                </div>
                <div>
                <a href="#" className="social-media-items__item">
                    <img src="/static/images/icon-facebook.png" alt="facebook" />
                </a>
                </div>
                <div>
                <a href="#" className="social-media-items__item">
                    <img src="/static/images/icon-pinterest.png" alt="pinterest" />
                </a>
                </div>
                <div>
                <a href="#" className="social-media-items__item">
                    <img src="/static/images/icon-google-plus.png" alt="google-plus" />
                </a>
                </div>
                <div>
                <a href="#" className="social-media-items__item">
                    <img src="/static/images/icon-linkedin.png" alt="linkedin" />
                </a>
                </div>
                <div>
                <a href="#" className="social-media-items__item">
                    <img src="/static/images/icon-you-tube.png" alt="you tube" />
                </a>
                </div>
                </Col>
            </Row>
      </Grid>
      </div>
    );
    
  }


  render() {
    return ( 
      <div>
      {this.showSocialMedia()} 
      </div>
    ); 
  }

}

export default SocialMedia;
