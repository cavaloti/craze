import {Component} from 'react';
import Head from '../components/Head';
import Header from '../components/Header';
import Features from '../components/Features';
import About from '../components/About';
import Carousel from '../components/Carousel';
import Pricing from '../components/Pricing';
import SocialMedia from '../components/SocialMedia';
import Footer from '../components/Footer';

class Index extends Component {

    render() {
      return (
        <div>
          <Head/>
          <Header/>
          <Features/>
          <About/>
          <Carousel/>
          <Pricing />
          <SocialMedia/>
          <Footer/>
        </div>
      );
    }
  }
  
  export default Index;